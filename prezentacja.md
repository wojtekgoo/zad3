---
author: Wojciech GUSZTYŁA
title: Nowa Zelandia
subtitle: moje ulubione miejsce na ziemi
date: 06.11.2022
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---


## 

![](mapa.png)


## Położenie geograficzne 

Nowa Zelandia to państwo wyspiarskie położone na południowo-zachodnim Pacyfiku, w Australazji, na południowy wschód od Australii. Składa się z dwóch głównych wysp (\textcolor{red}{Północnej} i \textcolor{blue}{Południowej}) oraz szeregu mniejszych, w tym wyspy Stewart i wysp Chatham. W skład Nowej Zelandii (a dokładniej w skład królestwa stowarzyszeniowego Nowej Zelandii, połączonego unią personalną z Wielką Brytanią i innymi królestwami stowarzyszeniowymi) wchodzą również terytoria stowarzyszone z Nową Zelandią lub od niej zależne: Wyspy Cooka i Niue, które są samorządne oraz Tokelau i Dependencja Rossa. Stolicą jest **Wellington**, natomiast największym miastem jest **Auckland**

## Grupy etniczne i religia

Ponad 80% ludnosci Nowej Zelandii jest pochodzenia europejskiego, a jezykiem urzedowym jest angielski. Liczba rdzennych mieszkanców jest niewielka, stanowia oni jedynie ok. 9% ludnosci wysp. Wiekszosc ludnosci mieszka w miastach: poziom urbanizacji jest tu bardzo wysoki, wynosi 85%.

Struktura religijna:

* brak religii – 48,2% 
* chrześcijaństwo – 37,0%
	* katolicy – 10,0%
	* anglikanie – 6,7%
	* nieokreśleni – 6,6%
* odmowa odpowiedzi – 6,7%

## Kultura

\begin{exampleblock}{Znane postaci}
Gwiazdą rozsławiającą Nową Zelandię na całym świecie jest z racji swego maoryskiego pochodzenia sopranistka Kiri Te Kanawa. Ponadto z Nowej Zelandii pochodzi Neil Finn, lider znanego rockowego zespołu Crowded House.
\end{exampleblock}

\begin{block}{Tradycje}
Do popularnych, równiez współczesnie, tradycji rdzennych mieszkanców należy wykonywanie tatuaży na twarzy, zwanych moko. Obecnie są one malowane, dawniej jednak robiono je poprzez nacinanie twarzy nożem.
\end{block}

\begin{exampleblock}{Muzyka}
Muzyka rdzennych mieszkanców wysp jest bardzo rytmiczna i prawie zawsze łączy sie także z tańcem, którego podstawą są stylizowane ruchy rąk i nóg.
\end{exampleblock}

## Kuchnia

Kuchnia Nowej Zelandii wciąż pozostaje pod dużym wpływem Wielkiej Brytanii, jednak ze względu na odmienny klimat, korzysta się tam z zupełnie innych składników. W nowozelandzkiej kuchni możemy znaleźć tradycyjne brytyjskie fish&chips, burgery i hotdogi, ale także niezwykle popularne sushi – wyjątkowo tanie ze względu na łatwy dostęp do świeżych ryb słono- i słodkowodnych. Dania serwowane w Nowej Zelandii to zatem połączenie egzotyki z europejskimi zwyczajami. 

![](kuchnia1.png){ height=40% width=30%}
![](kuchnia2.jpg){ height=40% width=30%}
![](kuchnia3.png){ height=40% width=30%}